use std::{
    fs::{read_to_string, File},
    io::Read,
    str::FromStr,
};

const IIO_PREFIX: &str = "/sys/bus/iio/devices/";

const IIO_ACCEL_MOUNT_MATRIX: &str = "in_accel_mount_matrix";
const IIO_ACCEL_X: &str = "in_accel_x_raw";
const IIO_ACCEL_X_BIAS: &str = "in_accel_x_calibbias";
const IIO_ACCEL_Y: &str = "in_accel_y_raw";
const IIO_ACCEL_Y_BIAS: &str = "in_accel_x_calibbias";
const IIO_ACCEL_Z: &str = "in_accel_z_raw";
const IIO_ACCEL_Z_BIAS: &str = "in_accel_x_calibbias";

// const IIO_ANGLVEL_MOUNT_MATRIX: &str = "in_anglvel_mount_matrix";
const IIO_ANGLVEL_X: &str = "in_anglvel_x_raw";
const IIO_ANGLVEL_X_BIAS: &str = "in_anglvel_x_calibbias";
const IIO_ANGLVEL_Y: &str = "in_anglvel_y_raw";
const IIO_ANGLVEL_Y_BIAS: &str = "in_anglvel_y_calibbias";
const IIO_ANGLVEL_Z: &str = "in_anglvel_z_raw";
const IIO_ANGLVEL_Z_BIAS: &str = "in_anglvel_z_calibbias";

const SAMPLES: usize = 11;
const CENTER: usize = 6;

pub struct IMU {
    _path: String,
    read_buffer: [u8; 64],
    sample_buffer: [[i16; SAMPLES]; 6],

    sampling_frequency: u64,

    acc_matrix: [[i16; 3]; 3],
    ang_matrix: [[i16; 3]; 3],

    iio_accel_x: String,
    iio_accel_y: String,
    iio_accel_z: String,

    iio_accel_matrix: String,

    _iio_accel_x_bias: String,
    _iio_accel_y_bias: String,
    _iio_accel_z_bias: String,

    iio_anglvel_x: String,
    iio_anglvel_y: String,
    iio_anglvel_z: String,

    _iio_anglvel_x_bias: String,
    _iio_anglvel_y_bias: String,
    _iio_anglvel_z_bias: String,
}

impl IMU {
    pub fn init() -> Option<Self> {
        let path = std::fs::read_dir(IIO_PREFIX)
            .unwrap()
            .map(|f| f.ok())
            .filter(Option::is_some)
            .map(Option::unwrap)
            .filter(|e| {
                e.file_name().to_str().is_some()
                    && e.file_name().to_str().unwrap().starts_with("iio:device")
            })
            .find_map(|e| {
                let mut path_buf = e.path();
                path_buf.push("name");
                if let Some(true) = read_to_string(&path_buf)
                    .ok()
                    .map(|n| n.starts_with("mpu6050"))
                {
                    Some(e.path())
                } else {
                    None
                }
            });

        if path.is_none() {
            return None;
        }
        let path = path.unwrap();
        let path = path.to_str();
        if path.is_none() {
            return None;
        }
        let path = path.unwrap();


        let mut read_buffer = [0u8; 64];
        let sampling_frequency: u64 = read_iio(&mut read_buffer, &format!("{}/{}", path, "sampling_frequency")).unwrap_or(50);

        Some(IMU {
            _path: path.into(),
            
            read_buffer,
            sample_buffer: [[0i16; SAMPLES]; 6],
            
            acc_matrix: [[0i16; 3]; 3],
            ang_matrix: [[0i16; 3]; 3],

            sampling_frequency,

            iio_accel_x: format!("{}/{}", path, IIO_ACCEL_X),
            iio_accel_y: format!("{}/{}", path, IIO_ACCEL_Y),
            iio_accel_z: format!("{}/{}", path, IIO_ACCEL_Z),
            iio_accel_matrix: format!("{}/{}", path, IIO_ACCEL_MOUNT_MATRIX),

            _iio_accel_x_bias: format!("{}/{}", path, IIO_ACCEL_X_BIAS),
            _iio_accel_y_bias: format!("{}/{}", path, IIO_ACCEL_Y_BIAS),
            _iio_accel_z_bias: format!("{}/{}", path, IIO_ACCEL_Z_BIAS),

            iio_anglvel_x: format!("{}/{}", path, IIO_ANGLVEL_X),
            iio_anglvel_y: format!("{}/{}", path, IIO_ANGLVEL_Y),
            iio_anglvel_z: format!("{}/{}", path, IIO_ANGLVEL_Z),

            _iio_anglvel_x_bias: format!("{}/{}", path, IIO_ANGLVEL_X_BIAS),
            _iio_anglvel_y_bias: format!("{}/{}", path, IIO_ANGLVEL_Y_BIAS),
            _iio_anglvel_z_bias: format!("{}/{}", path, IIO_ANGLVEL_Z_BIAS),
        })
    }

    fn to_micro_g(&self, value: i16, bias: i16) -> i16 {
        ((value - bias) as f64 / 16384.0 * 1000.0) as i16
    }

    fn to_degree_per_s(&self, value: i16, bias: i16) -> i16 {
        ((value - bias) as f64 / 16.4 ) as i16
        // value
    }

    pub fn read(&mut self) -> [i16; 6] {
        for i in 0..SAMPLES {
            let v = self.read_single();
            self.sample_buffer[0][i] = v[0];
            self.sample_buffer[1][i] = v[1];
            self.sample_buffer[2][i] = v[2];
            self.sample_buffer[3][i] = v[3];
            self.sample_buffer[4][i] = v[4];
            self.sample_buffer[5][i] = v[5];
            std::thread::sleep(std::time::Duration::from_micros(self.sampling_frequency))
        }
        self.sample_buffer[0].sort();
        self.sample_buffer[1].sort();
        self.sample_buffer[2].sort();
        self.sample_buffer[3].sort();
        self.sample_buffer[4].sort();
        self.sample_buffer[5].sort();

        let acc_mat = read_iio_matrix(&mut self.read_buffer, &self.iio_accel_matrix);

        let acc_x = self.sample_buffer[0][CENTER];
        let acc_y = self.sample_buffer[1][CENTER];
        let acc_z = self.sample_buffer[2][CENTER];
        
        // let acc_x = acc_mat[0]
        [
            acc_x,
            acc_y,
            acc_z,
            self.sample_buffer[3][CENTER],
            self.sample_buffer[4][CENTER],
            self.sample_buffer[5][CENTER],
        ]
    }

    fn read_single(&mut self) -> [i16; 6] {
        let acc_x: i16 = read_iio(&mut self.read_buffer, &self.iio_accel_x).unwrap_or(0);
        // let acc_x_bias: i16 = read_iio(&mut self.read_buffer, &self.iio_accel_x_bias).unwrap_or(0);
        let acc_y: i16 = read_iio(&mut self.read_buffer, &self.iio_accel_y).unwrap_or(0);
        // let acc_y_bias: i16 = read_iio(&mut self.read_buffer, &self.iio_accel_y_bias).unwrap_or(0);
        let acc_z: i16 = read_iio(&mut self.read_buffer, &self.iio_accel_z).unwrap_or(0);
        // let acc_z_bias: i16 = read_iio(&mut self.read_buffer, &self.iio_accel_z_bias).unwrap_or(0);

        let anglvel_x: i16 = read_iio(&mut self.read_buffer, &self.iio_anglvel_x).unwrap_or(0);
        // let anglvel_x_bias: i16 = read_iio(&mut self.read_buffer, &self.iio_anglvel_x_bias).unwrap_or(0);
        let anglvel_y: i16 = read_iio(&mut self.read_buffer, &self.iio_anglvel_y).unwrap_or(0);
        // let anglvel_y_bias: i16 = read_iio(&mut self.read_buffer, &self.iio_anglvel_y_bias).unwrap_or(0);
        let anglvel_z: i16 = read_iio(&mut self.read_buffer, &self.iio_anglvel_z).unwrap_or(0);
        // let anglvel_z_bias: i16 = read_iio(&mut self.read_buffer, &self.iio_anglvel_z_bias).unwrap_or(0);

        [
            self.to_micro_g(acc_x, 0),
            self.to_micro_g(acc_y, 0),
            self.to_micro_g(acc_z, 0),
            self.to_degree_per_s(anglvel_x, 0),
            self.to_degree_per_s(anglvel_y, 0),
            self.to_degree_per_s(anglvel_z, 0),
        ]
    }
}

fn read_iio<T: FromStr>(buf: &mut [u8], path: &str) -> Option<T> {
    match File::open(path) {
        Ok(mut file) => {
            let len = file.read(buf).unwrap();
            std::str::from_utf8(&buf[0..len])
                .ok()
                .map(|s| str::parse::<T>(&s.trim()).ok())
                .flatten()
        }
        Err(e) => {
            println!("{:?} {}", e, path);
            None
        }
    }
}

fn read_iio_matrix(buf: &mut [u8], path: &str) -> [[i16; 3]; 3] {
    let mut file = File::open(path).unwrap();
    let len = file.read(buf).unwrap();
    let mut matrix = [[0i16; 3]; 3];
    let string_matrix = std::str::from_utf8(&buf[0..len]).unwrap();
    for (i, string_vec) in string_matrix.split(";").enumerate() {
        for (j, value) in string_vec.split(",").enumerate() {
            matrix[i][j] = str::parse::<i16>(value.trim()).unwrap();
        }
    }
    drop(file);
    return matrix;
}
