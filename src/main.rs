mod imu;

fn main() {
    let mut acc = imu::IMU::init().unwrap();
    loop {
        let matrix = acc.read();
        print!("[");
        print!("{:#05},", matrix[0]);
        print!("{:#05},", matrix[1]);
        print!("{:#05},", matrix[2]);
        print!("{:#05},", matrix[3]);
        print!("{:#05},", matrix[4]);
        print!("{:#05}", matrix[5]);
        println!("]");
        std::thread::sleep(std::time::Duration::from_millis(10));
    }
}
